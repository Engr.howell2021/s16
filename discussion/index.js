console.log('hellow world');


// Repirition Control Structures

/*
	3 Types of looping Constructs

	1. While loop
	2. Do-while Loop
	3. For Loop
*/

/*
	While Loop
	-Syntax
		While(expression/condition) {
			statement/s;
		}
*/
// let count = 5;

// while(count !== 0) {
// 	console.log("while: " + count);
// 	count--; // count = count -1,
// }


let count = 0;

while(count < 5) {
	count++;
	console.log("while: " + count);
	 // count = count -1,
}

// DO-While

let num1 = 1;

do{
	console.log("Do-while: " + num1);
	num1++;
}while(num1 <= 6)


let num2 = Number(prompt("Give me a number"));

do{
	console.log("Do-while: " + num2);
	num2 += 1;
}while (num2 <10)


/*
	for Lopp
	syntax:

*/

for(let num3 =1; num3 <= 5; num3++){
	console.log(num3);
}


// console.log("for loop with prompt")


// let newNum = Number(prompt("Enter a number"));

// for newNum = num4; num4 <=3 numb4++ {
// 	console.log(num4);

// }


 let numb1 = Number(prompt("Give me a number"));

let i = 1;
for (i; i <= numb1; i++) {
     console.log(i);
    
    
}

let myString = "Juan" 
console.log(myString.length)

console.log(myString[0])
console.log(myString[2])
console.log(myString[3])

for(let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}


for(let count = 0; count <= 20; count++){
	if(count % 2 === 0) {
		continue;
	}

	console.log("Continue and break: " + count);

	if(count > 10){
		break;
	}

}
